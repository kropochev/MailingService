import logging
from datetime import datetime, timedelta

from typing import Dict, List
from fastapi import Depends, APIRouter, status, HTTPException
from fastapi.encoders import jsonable_encoder

from pydantic import BaseModel, Field
from sqlalchemy.orm import Session

import models
from worker import send_msg
from database import engine, SessionLocal


logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/mailings",
    tags=["mailings"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class MailingBase(BaseModel):
    start: datetime = Field(title="Start of the mailing")
    stop: datetime = Field(title="End of the mailing")
    text: str = Field(
        title="Text of the mailing",
        min_length=1,
        max_length=1000
        )
    filter: Dict[str, List] = Field(title="Filter of the mailing")

    class Config:
        schema_extra = {
            "example": {
                "start": datetime.now(),
                "stop": datetime.now() + timedelta(days=1),
                "text": "Text of the mailing",
                "filter": {
                    "mobile_code": [900],
                    "tags": ["tag1", "tag2"],
                }
            }
        }


class MailingCreate(MailingBase):
    pass


class Mailing(MailingBase):
    id: int

    class Config:
        orm_mode = True


@router.get("/", status_code=status.HTTP_200_OK)
async def read_all_mailings(db: Session = Depends(get_db)):

    mailing_list = db.query(models.Mailings).all()
    return mailing_list


@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_mailing(
    mailing: MailingCreate,
    db: Session = Depends(get_db)
):
    mailing_model = models.Mailings()
    mailing_model.start = mailing.start
    mailing_model.stop = mailing.stop
    mailing_model.text = mailing.text
    mailing_model.filter = jsonable_encoder(mailing.filter)

    db.add(mailing_model)
    db.commit()

    logger.info("Create mailing id: %d", mailing_model.id)

    await send_msg(mailing_model.id)

    return {"message": "Mailing added"}


@router.put("/", status_code=status.HTTP_200_OK)
async def update_mailing(
    mailing_id: int,
    mailing: MailingBase,
    db: Session = Depends(get_db)
):

    mailing_model = (
        db.query(models.Mailings)
        .filter(models.Mailings.id == mailing_id)
        .first()
    )
    if mailing_model is None:
        raise raise_mailing_not_found()

    logger.info("Update mailing id: %d", mailing_model.id)

    mailing_model.start = mailing.start
    mailing_model.stop = mailing.stop
    mailing_model.text = mailing.text
    mailing_model.filter = jsonable_encoder(mailing.filter)

    db.add(mailing_model)
    db.commit()
    return {"message": "Mailing updated"}


@router.delete("/", status_code=status.HTTP_200_OK)
async def delete_mailing(
    mailing_id: int,
    db: Session = Depends(get_db)
):

    mailing_model = (
        db.query(models.Mailings)
        .filter(models.Mailings.id == mailing_id)
        .first()
    )

    if mailing_model is None:
        raise raise_mailing_not_found()

    logger.info("Delete mailing id: %d", mailing_model.id)

    db.query(models.Mailings).filter(
        models.Mailings.id == mailing_id
    ).delete()
    db.commit()
    return {"message": "Mailing deleted"}


def raise_mailing_not_found():
    return HTTPException(
        status_code=400,
        detail="Mailing not found",
        headers={"X-Header-Error": "Nothing to be seen"},
    )
