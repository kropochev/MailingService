import logging
from datetime import datetime

from fastapi import Depends, APIRouter, status

from pydantic import BaseModel, Field
from sqlalchemy.orm import Session

import models
from database import engine, SessionLocal


logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/messages",
    tags=["messages"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class MessageBase(BaseModel):
    msg_datetime: datetime = Field(title="Date time of the message")
    msg_status: str = Field(title="Status of the message")
    mailing_id: int = Field(title="Mailing id of the message")
    client_id: int = Field(title="Client id of the message")

    class Config:
        schema_extra = {
            "example": {
                "msg_datetime": datetime.now(),
                "msg_status": "delivered",
                "mailing_id": 1,
                "client_id": 1
            }
        }


class MessageCreate(MessageBase):
    pass


class Message(MessageBase):
    id: int

    class Config:
        orm_mode = True


@router.get("/", status_code=status.HTTP_200_OK)
async def read_all_messages(db: Session = Depends(get_db)):

    messages_list = db.query(models.Messages).all()
    return messages_list


@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_message(
    message: MessageCreate,
    db: Session = Depends(get_db)
):

    message_model = models.Messages()
    message_model.datetime = message.msg_datetime
    message_model.status = message.msg_status
    message_model.mailing_id = message.mailing_id
    message_model.client_id = message.client_id

    db.add(message_model)
    db.commit()

    logger.info("Create message id: %d", message_model.id)

    return {"message": "Message added"}
