import os
import logging
from datetime import datetime

from arq import create_pool
from arq.connections import RedisSettings

import models
from database import SessionLocal


logger = logging.getLogger(__name__)

REDIS_HOST = os.environ.get('REDIS_HOST')
REDIS_PORT = os.environ.get('REDIS_PORT')


async def create_task(
    id: int,
    phone_number: int,
    text: str,
    start: datetime,
    data: dict
):

    redis = await create_pool(RedisSettings(host=REDIS_HOST, port=REDIS_PORT))
    start_at = (start - datetime.now()).total_seconds()

    await redis.enqueue_job(
        'send_message',
        id,
        phone_number,
        text,
        data,
        _defer_by=start_at
    )


async def send_msg(mailing_id: int):

    try:
        db = SessionLocal()

        mailing_model = (
            db.query(models.Mailings)
            .filter(models.Mailings.id == mailing_id)
            .first()
        )

        if mailing_model:
            mobile_code = mailing_model.filter.get("mobile_code")

            clients = (
                db.query(models.Clients)
                .filter(models.Clients.mobile_code == mobile_code[0])
                .all()
            )

            for client in clients:

                message_model = models.Messages()
                message_model.datetime = datetime.now()
                message_model.status = "in progress"
                message_model.client_id = client.id
                message_model.mailing_id = mailing_model.id
                db.add(message_model)
                db.commit()

                logger.info("Create message id: %d", message_model.id)

                await create_task(
                    id=message_model.id,
                    phone_number=client.phone_number,
                    text=mailing_model.text,
                    start=mailing_model.start,
                    data={"mailing_id": mailing_id, "client_id": client.id}
                )

    finally:
        db.close()
