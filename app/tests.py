from fastapi.testclient import TestClient

from main import app


client = TestClient(app)


def test_read_admin():
    response = client.get("/admin")
    assert response.status_code == 200


def test_create_mailing():
    example = {
        "start": "2022-04-03T17:58:18.571611",
        "stop": "2022-04-04T17:58:18.571618",
        "text": "Text of the mailing",
        "filter": {
            "mobile_code": [900],
            "tags": [
                "tag1",
                "tag2",
            ]
        }
    }
    response = client.post("/mailings/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Mailing added"}


def test_create_client():
    example = {
        "phone_number": 79001234567,
        "mobile_code": 900,
        "tag": "tag1",
        "time_zone": 3
    }
    response = client.post("/clients/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Client created"}


def get_stat1():
    response = client.get("/stats/")
    assert response.status_code == 200
    assert response.json() == {
        "mailings": 1,
        "messages": 0
    }


def test_update_mailing():
    example = {
        "start": "2022-04-03T17:58:18.571611",
        "stop": "2022-04-04T17:58:18.571618",
        "text": "Text of the mailing",
        "filter": {
            "mobile_code": [901],
            "tags": [
                "tag1",
                "tag2",
                "tag3",
            ]
        }
    }
    response = client.put("/mailings/?mailing_id=1", json=example)
    assert response.status_code == 200
    assert response.json() == {"message": "Mailing updated"}


def test_update_client():
    example = {
        "id": 1,
        "phone_number": 79001234567,
        "mobile_code": 900,
        "tag": "tag3",
        "time_zone": 3
    }
    response = client.put("/clients/", json=example)
    assert response.status_code == 200
    assert response.json() == {"message": "Client updated"}


def test_delete_mailing():
    response = client.delete("/mailings/?mailing_id=1")
    assert response.status_code == 200
    assert response.json() == {"message": "Mailing deleted"}


def test_delete_client():
    response = client.delete("/clients/?client_id=1")
    assert response.status_code == 200
    assert response.json() == {"message": "Client deleted"}


def get_stat2():
    response = client.get("/stats/")
    assert response.status_code == 200
    assert response.json() == {
        "mailings": 0,
        "messages": 0
    }


def test_create_mailing2():
    example = {
        "start": "2022-04-03T17:58:18.571611",
        "stop": "2022-04-04T17:58:18.571618",
        "text": "Text of the mailing",
        "filter": {
            "mobile_code": [900],
            "tags": [
                "tag1",
                "tag2",
            ]
        }
    }
    response = client.post("/mailings/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Mailing added"}


def test_create_client2():
    example = {
        "phone_number": 79001234567,
        "mobile_code": 900,
        "tag": "tag1",
        "time_zone": 3
    }
    response = client.post("/clients/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Client created"}


def test_create_message_in_progress():
    example = {
        "msg_datetime": "2022-04-03T18:26:03.603265",
        "msg_status": "in progress",
        "mailing_id": 2,
        "client_id": 2
    }
    response = client.post("/messages/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Message added"}


def test_create_message_delivered():
    example = {
        "msg_datetime": "2022-04-03T18:26:03.603265",
        "msg_status": "delivered",
        "mailing_id": 2,
        "client_id": 2
    }
    response = client.post("/messages/", json=example)
    assert response.status_code == 201
    assert response.json() == {"message": "Message added"}


def get_stat3():
    response = client.get("/stats/")
    assert response.status_code == 200
    assert response.json() == {
        "mailings": 1,
        "messages": 2
    }
